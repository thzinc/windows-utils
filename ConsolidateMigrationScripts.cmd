@echo off
setlocal
set branch=%1
mkdir tmp 2> nul
call DatabaseTool.cmd --dryRun --migrate -q StagingPre --types PreOffline --dump tmp\scorebig-%branch%-pre-offline.sql
call DatabaseTool.cmd --dryRun --migrate -q StagingPre --types Pre --dump tmp\scorebig-%branch%-pre.sql
call DatabaseTool.cmd --dryRun --migrate -q StagingPre --types Post --dump tmp\scorebig-%branch%-post.sql
call DatabaseTool.cmd --dryRun --migrate -q StagingPre --types Rollback --dump tmp\scorebig-%branch%-rollback.sql
call DatabaseTool.cmd --dryRun --migrate -q StagingPre --types Cleanup --dump tmp\scorebig-%branch%-cleanup.sql
move tmp\* .\
rmdir tmp
