param(
[string]$privateQueueName = "servicebusmessages"
)
$queuename = ".\private$\" + $privateQueueName
[Reflection.Assembly]::LoadWithPartialName("System.Messaging") | Out-Null
$queue = New-Object -TypeName "System.Messaging.MessageQueue"
$queue.Path = $queuename
$messagecount = $queue.GetAllMessages().Length
$queue.Purge()
Write-Host "$queuename has been purged of $messagecount messages." -ForegroundColor Yellow