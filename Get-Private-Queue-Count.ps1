param(
[string]$privateQueueName = "servicebusmessages"
)
$queuename = ".\private$\" + $privateQueueName
[Reflection.Assembly]::LoadWithPartialName("System.Messaging") | Out-Null
$queue = New-Object -TypeName "System.Messaging.MessageQueue"
$queue.Path = $queuename
$messagecount = $queue.GetAllMessages().Length
Write-Host "$queuename has $messagecount messages."